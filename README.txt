README.txt
==========

The Custom Notifications module is a powerful and flexible solution for managing
and sending personalized notifications to users in your Drupal site. With this
module, site administrators can easily create custom notifications to be
delivered via email.
