<?php

namespace Drupal\custom_notifications\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for handling user autocomplete.
 */
class AutocompleteUsersController extends ControllerBase {

  /**
   * Returns a JSON response with matching usernames or mails.
   */
  public function usersAutocomplete(Request $request) {
    $matches = [];

    $input = $request->query->get('q');
    if ($input) {
      $query = $this->entityTypeManager()
        ->getStorage('user')
        ->getQuery();

      $orCondition = $query->orConditionGroup()
        ->condition('name', $input, 'CONTAINS')
        ->condition('mail', $input, 'CONTAINS');
      // Search for users whose names or email addresses match the input.
      $query->condition($orCondition)
      // Limit to active users.
        ->condition('status', 1)
        ->sort('name')
        ->sort('mail')
      // Limit the number of results to 10.
        ->range(0, 10);

      $uids = $query->execute();
      if ($uids) {
        $users = $this->entityTypeManager()->getStorage('user')->loadMultiple($uids);

        foreach ($users as $user) {
          $matches[] = [
            'value' => $user->get('mail')->getString(),
            'label' => $user->get('mail')->getString(),
          ];
        }
      }
    }

    return new JsonResponse($matches);
  }

}
