<?php

namespace Drupal\custom_notifications\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\custom_notifications\EntityEvents;

/**
 * Custom Notifications form.
 *
 * @property \Drupal\custom_notifications\CustomNotificationsInterface $entity
 */
class CustomNotificationsForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Define a label of custom notification.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\custom_notifications\Entity\CustomNotifications::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the custom notification.'),
    ];

    $form['collapsible_message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message details'),
      '#open' => TRUE,
    ];

    $form['collapsible_message']['message_title'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Title'),
      '#default_value' => $this->entity->get('message_title') ?? '',
      '#description' => $this->t('Write a title to be sent as a title notification.'),
    ];

    $message = $this->entity->get('message');
    $form['collapsible_message']['message'] = [
      '#type' => 'text_format',
      '#format' => $message['format'] ?? 'full_html',
      '#title' => $this->t('Message'),
      '#default_value' => $message['value'] ?? '',
      '#description' => $this->t('Write a message to be sent as a body notification.'),
    ];

    $nodeTypeList = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $types = [];
    foreach ($nodeTypeList as $nodeType) {
      $types[$nodeType->id()] = $nodeType->label();
    }
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content Types'),
      '#default_value' => $this->entity->getContentTypes() ?? [],
      '#options' => $types,
      '#required' => TRUE,
      '#description' => $this->t('Select one or more content type types.'),
    ];

    $form['action_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Actions'),
      '#default_value' => $this->entity->getActionTypes() ?? [],
      '#options' => [
        EntityEvents::CREATE => $this->t('Create'),
        EntityEvents::UPDATE => $this->t('Update'),
        EntityEvents::DELETE => $this->t('Delete'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('Select one or more actions to notify when an content type is created, modified or deleted.'),
    ];

    $form['collapsible_users'] = [
      '#type' => 'details',
      '#title' => $this->t('User List'),
      '#open' => TRUE,
    ];

    $form['collapsible_users']['users'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Users'),
      '#default_value' => $this->entity->getUsers() ?? [],
      '#autocomplete_route_name' => 'custom_notification.users_autocomplete',
      '#multiple' => TRUE,
      '#tags' => TRUE,
    ];

    $form['collapsible_roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Role List'),
      '#open' => TRUE,
    ];

    $roleList = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $roles = [];
    foreach ($roleList as $role) {
      $roles[$role->id()] = $role->label();
    }
    $form['collapsible_roles']['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => $this->entity->getRoles() ?? [],
      '#options' => $roles,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
        ? $this->t('Created new custom notifications %label.', $message_args)
        : $this->t('Updated custom notifications %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
