<?php

namespace Drupal\custom_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *  The config form to Custom Notification module.
 */
class CustomNotificationSettingsForm extends ConfigFormBase{


  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_notifications.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'custom_notifications.settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state){
    $config = $this->config('custom_notifications.settings');
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->get('label'),
      '#description' => $this->t('Define a label of custom notification.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->get('id'),
      '#machine_name' => [
        'exists' => '\Drupal\custom_notifications\Entity\CustomNotifications::load',
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('description'),
      '#description' => $this->t('Description of the custom notification.'),
    ];

    $form['collapsible_message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message details'),
      '#open' => TRUE,
    ];

    $form['collapsible_message']['message_title'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('message_title') ?? '',
      '#description' => $this->t('Write a title to be sent as a title notification.'),
    ];

    $message = $config->get('message');
    $form['collapsible_message']['message'] = [
      '#type' => 'text_format',
      '#format' => $message['format'] ?? 'full_html',
      '#title' => $this->t('Message'),
      '#default_value' => $message['value'] ?? '',
      '#description' => $this->t('Write a message to be sent as a body notification.'),
    ];

    return parent::buildForm($form, $form_state);
  }

    /**
     * {@inheritDoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $this->config('custom_notifications.settings')
        ->set('label', $form_state->getValue('label'))
        ->set('id', $form_state->getValue('id'))
        ->set('description', $form_state->getValue('description'))
        ->set('collapsible_message', $form_state->getValue('collapsible_message'))
        ->set('message', $form_state->getValue('message'))
        ->set('message_title', $form_state->getValue('message_title'))
        ->save();
    }
}
?>
