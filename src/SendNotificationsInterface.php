<?php

namespace Drupal\custom_notifications;

use Drupal\Core\Entity\EntityInterface;
use Drupal\custom_notifications\Entity\CustomNotifications;

/**
 * Custom Notifications Subscriber Interface.
 */
interface SendNotificationsInterface {

  /**
   * Key to identify the notification.
   */
  public const MAIL_KEY = 'custom.notification.mail.key';

  /**
   * Send notification.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\custom_notifications\Entity\CustomNotifications $custom_notifications
   *   The custom notifications entity.
   *
   * @return bool
   *   Return TRUE if the notification was sent, otherwise FALSE.
   */
  public function sendNotification(EntityInterface $entity, CustomNotifications $custom_notifications): bool;

}
