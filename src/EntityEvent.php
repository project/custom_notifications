<?php

namespace Drupal\custom_notifications;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\custom_notifications\Entity\CustomNotifications;

/**
 * Defines a base class for all entity events.
 */
class EntityEvent extends Event {

  /**
   * The entity object.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The user list.
   *
   * @var string
   */
  protected $users;

  /**
   * The role list.
   *
   * @var array
   */
  protected $roles;

  /**
   * The custom notifications entity.
   *
   * @var \Drupal\custom_notifications\Entity\CustomNotifications
   */
  protected $customNotifications;

  /**
   * EntityEvent constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   * @param \Drupal\custom_notifications\Entity\CustomNotifications $custom_notifications
   *   Custom notifications object.
   */
  public function __construct(EntityInterface $entity, CustomNotifications $custom_notifications) {
    $this->entity = $entity;
    $this->customNotifications = $custom_notifications;
  }

  /**
   * Get an entity object.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity object.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Get user list.
   *
   * @return \Drupal\custom_notifications\Entity\CustomNotifications
   *   Custom notifications entity.
   */
  public function getCustomNotifications() {
    return $this->customNotifications;
  }

}
