<?php

namespace Drupal\custom_notifications\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\custom_notifications\CustomNotificationsInterface;

/**
 * Defines the custom notifications entity type.
 *
 * @ConfigEntityType(
 *   id = "custom_notifications",
 *   label = @Translation("Custom Notifications"),
 *   label_collection = @Translation("Custom Notificationss"),
 *   label_singular = @Translation("custom notifications"),
 *   label_plural = @Translation("custom notificationss"),
 *   label_count = @PluralTranslation(
 *     singular = "@count custom notifications",
 *     plural = "@count custom notificationss",
 *   ),
 *   handlers = {
 *     "list_builder" =
 *   "Drupal\custom_notifications\CustomNotificationsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\custom_notifications\Form\CustomNotificationsForm",
 *       "edit" = "Drupal\custom_notifications\Form\CustomNotificationsForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "custom_notifications",
 *   admin_permission = "administer custom_notifications",
 *   links = {
 *     "collection" = "/admin/structure/custom-notifications",
 *     "add-form" = "/admin/structure/custom-notifications/add",
 *     "edit-form" =
 *   "/admin/structure/custom-notifications/{custom_notifications}",
 *     "delete-form" =
 *   "/admin/structure/custom-notifications/{custom_notifications}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "status",
 *     "content_types",
 *     "action_types",
 *     "users",
 *     "roles",
 *     "message_title",
 *     "message",
 *   }
 * )
 */
class CustomNotifications extends ConfigEntityBase implements CustomNotificationsInterface {

  /**
   * The custom notifications ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The custom notifications label.
   *
   * @var string
   */
  protected $label;

  /**
   * The custom notifications status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The custom_notifications description.
   *
   * @var string
   */
  protected $description;

  /**
   * The selected entity types.
   *
   * @var array
   */
  protected $content_types;

  /**
   * The selected users.
   *
   * @var string
   */
  protected $users;

  /**
   * The selected roles.
   *
   * @var array
   */
  protected $roles;

  /**
   * The selected actions.
   *
   * @var array
   */
  protected $action_types;

  /**
   * The message title.
   *
   * @var string
   */
  protected $message_title;

  /**
   * The message.
   *
   * @var array
   */
  protected $message;

  /**
   * {@inheritdoc}
   */
  public function setContentTypes(array $content_types) {
    $this->content_types = $content_types;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentTypes() {
    return $this->content_types;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsers(string $users) {
    $this->users = $users;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsers() {
    return $this->users;
  }

  /**
   * {@inheritdoc}
   */
  public function setRoles(array $roles = []) {
    $this->roles = $roles;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles() {
    return $this->roles;
  }

  /**
   * {@inheritdoc}
   */
  public function setActionTypes(array $action_types) {
    $this->action_types = $action_types;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getActionTypes() {
    return $this->action_types;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessage(array $message) {
    $this->message = $message;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessageTitle(string $message_title) {
    $this->message_title = $message_title;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageTitle() {
    return $this->message_title;
  }

}
