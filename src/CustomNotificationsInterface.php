<?php

namespace Drupal\custom_notifications;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a custom notifications entity type.
 */
interface CustomNotificationsInterface extends ConfigEntityInterface {

  /**
   * Set content types.
   *
   * @param array $content_types
   *   List of selected content types.
   *
   * @return $this
   */
  public function setContentTypes(array $content_types);

  /**
   * Get content types list.
   *
   * @return array
   *   List of content types.
   */
  public function getContentTypes();

  /**
   * Set action types.
   *
   * @param array $action_types
   *   Actions.
   *
   * @return $this
   */
  public function setActionTypes(array $action_types);

  /**
   * Get action types.
   *
   * @return array
   *   Action types.
   */
  public function getActionTypes();

  /**
   * Set users.
   *
   * @param string $users
   *   List of selected users.
   *
   * @return $this
   */
  public function setUsers(string $users);

  /**
   * Get users.
   *
   * @return array
   *   User list.
   */
  public function getUsers();

  /**
   * Set roles.
   *
   * @param array $roles
   *   List of selected roles.
   *
   * @return $this
   */
  public function setRoles(array $roles = []);

  /**
   * Get roles.
   *
   * @return array
   *   Roles.
   */
  public function getRoles();

  /**
   * Set message title.
   *
   * @param string $message_title
   *   Message title.
   *
   * @return $this
   */
  public function setMessageTitle(string $message_title);

  /**
   * Get message title.
   *
   * @return string
   *   Message title.
   */
  public function getMessageTitle();

  /**
   * Set message.
   *
   * @param array $message
   *   Message.
   *
   * @return $this
   */
  public function setMessage(array $message);

  /**
   * Get message.
   *
   * @return string
   *   Message value.
   */
  public function getMessage();

}
