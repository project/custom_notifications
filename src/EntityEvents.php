<?php

namespace Drupal\custom_notifications;

/**
 * Contains all events thrown while handling entities.
 */
final class EntityEvents {

  /**
   * The name of the event triggered when a new entity is created.
   *
   * This event allows modules to react to a new entity being created. The
   * event listener method receives a \Drupal\Core\Entity\EntityTypeEvent
   * instance.
   *
   * @Event
   *
   * @see \Drupal\custom_notifications\EntityEvent
   *
   * @var string
   */
  public const CREATE = 'entity_hook_create';

  /**
   * The name of the event triggered when an existing entity is updated.
   *
   * This event allows modules to react whenever an existing entity is
   * updated. The event listener method receives a
   * \Drupal\Core\Entity\EntityTypeEvent instance.
   *
   * @Event
   *
   * @see \Drupal\custom_notifications\EntityEvent
   *
   * @var string
   */
  public const UPDATE = 'entity_hook_update';

  /**
   * The name of the event triggered when an existing entity is deleted.
   *
   * This event allows modules to react whenever an existing entity is
   * deleted.  The event listener method receives a
   * \Drupal\Core\Entity\EntityTypeEvent instance.
   *
   * @Event
   *
   * @see \Drupal\custom_notifications\EntityEvent
   *
   * @var string
   */
  public const DELETE = 'entity_hook_delete';

}
