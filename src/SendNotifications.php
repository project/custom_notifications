<?php

namespace Drupal\custom_notifications;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\custom_notifications\Entity\CustomNotifications;
use Drupal\user\Entity\User;

/**
 * Send notifications service.
 */
class SendNotifications implements SendNotificationsInterface {

  use LoggerChannelTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a SendNotifications object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager, RendererInterface $renderer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inerhitdoc}
   */
  public function sendNotification(EntityInterface $entity, CustomNotifications $custom_notifications): bool {
    try {
      $mails = explode(',', $custom_notifications->getUsers());
      if ($roles = $custom_notifications->getRoles()) {
        $userStorage = $this->entityTypeManager->getStorage('user');
        $userIds = $userStorage
          ->getQuery()
          ->condition('roles', $roles, 'IN')
          ->accessCheck()
          ->execute();
        if ($userIds) {
          $userList = $userStorage->loadMultiple($userIds);
          foreach ($userList as $user) {
            if ($user instanceof User) {
              $mail = $user->getEmail();
              if (!in_array($mail, $mails, TRUE)) {
                $mails[] = $mail;
              }
            }
          }
        }
      }
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
      $body = [
        '#theme' => 'custom_notifications_message',
        '#message' => $custom_notifications->getMessage()['value'],
      ];
      $params = [
        'subject' => $custom_notifications->getMessageTitle(),
        'body' => [$this->renderer->render($body)],
      ];
      foreach ($mails as $mail) {
        $this->mailManager->mail('custom_notifications', self::MAIL_KEY, $mail, $langcode, $params);
      }
      return TRUE;
    }
    catch (\Exception $exception) {
      $this->getLogger('custom_notifications')->error($exception);
    }
    return FALSE;
  }

}
