<?php

namespace Drupal\Tests\custom_notifications\Functional;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Logger\LoggerChannel;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\custom_notifications\Entity\CustomNotifications;
use Drupal\custom_notifications\EntityEvents;
use Drupal\custom_notifications\SendNotifications;
use Drupal\Tests\BrowserTestBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tests the SendNotifications service.
 *
 * @group your_module
 */
class SendNotificationsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable for this test.
   *
   * @var array
   */
  protected static $modules = ['custom_notifications', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // parent::setUp();
    $logger_channel = $this->getMockBuilder(LoggerChannel::class)
      ->setConstructorArgs([
        "custom_notifications",
      ])
      ->getMock();
    $logger = $this->createMock(LoggerChannelFactory::class);
    $logger->expects($this->once())
      ->method('get')
      ->willReturn($logger_channel);
    $container = $this->getMockBuilder(ContainerBuilder::class)
      ->onlyMethods(['get'])
      ->getMock();
    $container->expects($this->once())
      ->method('get')
      ->with($this->equalTo('logger.factory'))
      ->willReturn($logger);

    \Drupal::setContainer($container);
  }

  /**
   * Tests the sendNotification method.
   */
  public function testSendNotification() {
    // Mock the required services.
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $mailManager = $this->createMock(MailManagerInterface::class);
    $languageManager = $this->createMock(LanguageManagerInterface::class);
    $language = $this->createMock(Language::class);
    $languageManager->method('getCurrentLanguage')->willReturn($language);
    $renderer = $this->createMock(RendererInterface::class);

    // Create the SendNotifications service.
    $sendNotifications = new SendNotifications($entityTypeManager, $mailManager, $languageManager, $renderer);

    // Create a mock entity and custom notification object.
    $entity = $this->getMockForAbstractClass(EntityInterface::class);
    $customNotifications = $this->getMockBuilder(CustomNotifications::class)
      ->setConstructorArgs([
        [
          "id" => 'testing_notification',
          "label" => 'Testing Notification',
          "description" => 'Notification desc',
          "status" => 1,
          "content_types" => ['page', 'article'],
          "action_types" => [EntityEvents::CREATE],
          "users" => 'test@example.com, editor@example.com',
          "roles" => NULL,
          "message_title" => 'Test Notification',
          "message" => [
            'value' => '<p>Test notification body</p>',
            'format' => 'full_html',
          ],
        ],
        'custom_notifications',
      ])
      ->getMock();

    // Call the sendNotification method and check the result.
    $result = $sendNotifications->sendNotification($entity, $customNotifications);
    $this->assertFalse($result);
  }

}

